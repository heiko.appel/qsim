#!/usr/bin/env python3
# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:hydrogen
#     text_representation:
#       extension: .py
#       format_name: hydrogen
#       format_version: '1.3'
#       jupytext_version: 1.10.3
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# %% [markdown]
# Copyright (c) 2004, 2021, Heiko Appel
# Copyright (c) 2021, Nicolas Tancogne-Dejean
# Copyright (c) 2021, Martin Lueders
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.

# %% [markdown]
# # Numerical Accuracy (1)
#
# ## Summation of series
#
#
# The question was to determine the number of terms, necessary to evaluate the
# sum
#
# $$
#   S_n(x) :=  \sum_{\nu=0}^{n-1} (-1)^{\nu}\frac{x^{2\nu+1}}{2\nu+1} , |x|\leq1
# $$
#
# with a _relative_ error of less than $10^{-6}$. Therefore we try to find that
# value of $n$ for which
#
# $$
#   S_n(x) :=  \sum_{\nu=0}^{n-1} (-1)^{\nu}\frac{x^{2\nu+1}}{2\nu+1} , |x|\leq1
# $$
#
# gives the value to the required accuracy.
#
# The function `qsim.exercise01.arctan()` calculates and prints the partial
# sums for various values of $n$.

# %% [markdown]
# Now, load the necessary modules:
# %%
import numpy as np
import matplotlib.pyplot as plt

from qsim.utils import display_source

import qsim.numerical_accuracy_1_master as exercise

# %%
display_source(exercise.arctan_approx_series_simple)

# %%
exercise.arctan_approx_series_simple(1.0, 1e-6)

# %% [markdown]
# This shows that 318310 are required.
#
#
# In the solution we also implemented a verbose mode, which can print a
# selection of terms.
#
# %%
exercise.arctan_approx_series_simple(1.0, 1e-6, True, 318300)

# %% [markdown]
# This function is very inefficient. It requires a huge number of terms, but
# also expensive operations for each term (such as the power operation). This
# function can be (slightly) improved by a version where the terms are
# successively build up from the last iteration, by only multiplying the
# numerator by $x^2$ and adding 2 to the denominator.
# %%
display_source(exercise.arctan_approx_series)

# %% [markdown]
# We can measure the performance by using the `%timeit` function of iPython:

# %%
%timeit  exercise.arctan_approx_series_simple(1.0, 1e-6)
# %%
%timeit  exercise.arctan_approx_series(1.0, 1e-6)

# %% [markdown]
# Let's also check that the approximating function gives the correct results

# %%
x_range = np.linspace(-1, 1, 100)

plt.rcParams["figure.dpi"] = 120
plt.plot(
    x_range,
    [exercise.arctan_approx_series(x, 1e-6)[0] for x in x_range],
    label="series",
)
plt.plot(x_range, np.arctan(x_range), "--", label="exact")
plt.xlabel("x")
plt.ylabel("arctan(x)")
plt.legend()
plt.show()

# %% [markdown]
# We can investigate, how the number of required terms behaves as function of the $x$:
# %%
x_range = np.linspace(-1, 1, 100)

plt.plot(x_range, [exercise.arctan_approx_series(x, 1e-6)[2] for x in x_range])
plt.yscale("log")
plt.xlabel("x")
plt.ylabel("Number of terms")
plt.show()

# %% [markdown]
#
# The series, as implemented, was only defined for $|x| < 1$, which means, that
# for $|x| = 1$ the limit of the convergence radius is reached. Approaching
# this limit, the convergence slows down, and beyond the convergence radius,
# the series will actually diverge.
#
# Expansions with a different range of convergence can be obtained e.g. by a
# Taylor expansion around different points.

# %% [markdown]
# Starting with the second term, every term requires 3 floating point
# operations (FLOPS), and one further operation to add it to the previous sum.
# This means, we need  $(318310-1)\times 3= 954927$ to achieve a precision of
# $10^{-6}$.
#
# The speedup was about a factor of 2, but the main problem is the number of
# required terms.  This can be addressed by the alternative formulation of the
# approximation in terms of the arithmetic-geometric means:

# %%
display_source(exercise.arctan_approx_means)

# %% [markdown]
# Note that in this routine, the stopping criterion is defined in terms of
# internal quantity, as usually the exact result is not available as a
# reference. For arithmetic-geometric means series like this, the two values
# $a_n$ and $b_n$ are equal in the limit $n \to \infty$, and hence their
# distance can be taken as a measure of the precision.


# %%
exercise.arctan_approx_means(1.0, 1e-6, True, 1)

# %% [markdown]
# If you look at the relative error, printed in the verbose output, you can see
# that the results is actually slightly overconverged, and one could have
# stopped one step earlier

# %% [markdown]
# It is not surprising, that this is also reflected in a dramatic improvement
# of the performance
# %%
%timeit  exercise.arctan_approx_means(1.0, 1e-6)

# %% [markdown]
# We can also check here, that this approximation gives the correct result,
# also outside the $|x|<1$ region:

# %%
x_range_2 = np.linspace(-10, 10, 1000)

plt.plot(
    x_range_2,
    [exercise.arctan_approx_means(x, 1e-6)[0] for x in x_range_2],
    label="means",
)
plt.plot(x_range_2, np.arctan(x_range_2), "--", label="exact")
plt.xlabel("x")
plt.ylabel("arctan(x)")
plt.legend()
plt.show()

# %% [markdown]
#
# It is also interesting to see how the two methods scale with the required
# precision.  For this we can generate an array of precision values


# %%
precisions = [pow(10, -h) for h in np.arange(0, 7, 0.5)]

plt.plot(
    precisions,
    [exercise.arctan_approx_series(1, prec)[2] for prec in precisions],
    label="series",
)
plt.plot(
    precisions,
    [exercise.arctan_approx_means(1, prec)[2] for prec in precisions],
    label="means",
)
plt.xscale("log")
plt.xlabel("precision")
plt.ylabel("number of terms")
plt.yscale("log")
plt.legend()
plt.show()


# %% [markdown]
#
# It can be seen that the `means` based approximation shows a much more
# favourable scaling with the desired accuracy.
#

# %%
x_range = np.linspace(-1, 1, 100)

plt.plot(
    x_range,
    [exercise.arctan_approx_series(x, 1e-6)[2] for x in x_range],
    label="series",
)
plt.plot(
    x_range_2,
    [exercise.arctan_approx_means(x, 1e-6)[2] for x in x_range_2],
    label="means",
)
plt.yscale("log")
plt.xlabel("x")
plt.ylabel("Number of terms")
plt.legend()
plt.show()

# %% [markdown]
# This shows, that the latter method also shows a much more uniform convergence
# behaviour than the simple series expansion.


# %% [markdown]
#
# ## Floating point arithmetics
#
# The machine accuracy can be determined with the following small program:

# %%
display_source(exercise.eps)

# %% [markdown]
# The machine precision values for the given starting points are:

# %%
for alpha in (1, 1e-4, 1e11):
    myeps = exercise.eps(alpha)
    print("{:e}\t {:d}\t \033[32m{:e}\033[0m ".format(alpha, myeps[1], myeps[0]))

# %% [markdown]
#
# The machine precision `eps` denotes the distance between a number $\alpha$
# and the machine number directly next to $\alpha$. It therefore provides a
# measure for the density of numbers on the number line in the vicinity of
# $\alpha$.  The calculation of `eps` for different values of $\alpha$ shows
# that numbers are denser in the region of small numbers and more widely spaced
# for large numbers.
