#!/usr/bin/env python3
# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:hydrogen
#     text_representation:
#       extension: .py
#       format_name: hydrogen
#       format_version: '1.3'
#       jupytext_version: 1.10.3
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# %% [markdown]
# Copyright (c) 2004, 2021, Heiko Appel
# Copyright (c) 2021, Nicolas Tancogne-Dejean
# Copyright (c) 2021, Martin Lueders
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.

# %% [markdown]
# # Polynomial interpolation
#
# ## Part 1)
#
# Interval division:
#
# \begin{eqnarray}
#   \Delta_4 &=& \left\{ x_0^{(4)},x_1^{(4)},x_2^{(4)},x_3^{(4)},x_4^{(4)} \right\} \\
#            &=& \left\{ -1,-1/2,0,1/2,1\right\}.
# \end{eqnarray}
#
# Table of dividing differences
#
# |     | $k = 0$ | $k = 1$ | $k = 2$ | $k = 3$ | $k = 4$ |
# | --- | --- | --- | --- | --- | --- |
# | $x_0^{(4)}=-1$ | $f_0=\frac{5}{26}$ | | | | |
# |     |      | $f_{01}=\frac{375}{377}$ | | | |
# | $x_1^{(4)}=-\frac{1}{2}$ | $f_1=\frac{20}{29}$ | | $f_{012}=\frac{2875}{377}$ | |
# | | | $f_{12}=\frac{250}{29}$ | | $f_{0123}=-\frac{6250}{377}$ | |
# | $x_2^{(4)}=0$ | $f_2=5$ |  | $f_{123}=-\frac{500}{29}$ |  | $f_{01234}=\frac{6250}{377}$ |
# |        |        | $f_{23}=-\frac{250}{29}$ |       | $f_{1234}=\frac{6250}{377}$   |  |
# |$x_3^{(4)}=\frac{1}{2}$ | $f_3=\frac{20}{29}$ |  | $f_{234}=\frac{2875}{377}$ | | |
# |               |               | $f_{34}=-\frac{375}{377}$ |       |            |  |
# | $x_0^{(4)}=1$   | $f_4=\frac{5}{26}$ |             |             |            |  |
#
# Newton form of the interpolating polynomial
# \begin{eqnarray}
# P_4(x) &=& f_0 + (x-x_0^{(4)})\left\{ f_{01}+ (x-x_1^{(4)})\left[ f_{012}+ (x-x_2^{(4)})
#         ( f_{0123}+ (x-x_3^{(4)})f_{0123}) \right] \right\} \nonumber \\
#        &=& \frac{5}{26} + (x+1)\left\{\frac{375}{377}+ (x+0.5)\left[\frac{2875}{377}+ x \cdot
#         (-\frac{6250}{377}+ (x-0.5)\frac{6250}{377}) \right] \right\} \nonumber
# \end{eqnarray}
#

# %% [markdown]
# ## Part 2)
#
# In this part, we will compare the performance of the Newton interpolation
# scheme for various number of support nodes, and shapes of the support grid.
# We use the following distributions:
#
# (i) $x_i^{(n)} = a + ih, \qquad$ with $\qquad h=\frac{b-a}{n}, \qquad$ for $\quad i=0,...,n$
#
# (ii) $x_i^{(n)} = -\cos \left( \frac{2i+1}{n+1} \frac{\pi}{2} \right) \qquad$ for $\quad i=0,...,n$
#
# %% [markdown]
# Load required libraries and set plot size:

# %%
import matplotlib.pyplot as plt
import numpy as np

from qsim.utils import display_source

import qsim.polynomial_interpolation_master as exercise

# %%
display_source(exercise.create_interval_i)

# %%
display_source(exercise.create_interval_ii)

# %% [markdown]
# Create a (pseudo-) continues range of x- and function-values for plotting the
# results:

# %%
x_cont = np.arange(-1, 1, 0.01)
f_cont = [exercise.f(x) for x in x_cont]

# %% [markdown]
# The interpolation is done with the function

# %%
display_source(exercise.interpolate)

# %% [markdown]
# Once the interpolation is done, we can use the Horner scheme to evaluate the
# polynomial.

# %%
display_source(exercise.horner)

# %% [markdown]
# ## Part 3)
#
# Calculate the interpolating polynomial for $\Delta_{5}$

# %%
Delta_i = exercise.create_interval_i(5)
Delta_ii = exercise.create_interval_ii(5)

# %%
coeffs_i = exercise.interpolate(exercise.f, Delta_i)
coeffs_ii = exercise.interpolate(exercise.f, Delta_ii)

# %% [markdown]
# The resulting coefficients are:

# %%
for i in range(0, len(Delta_i)):
    print("\t a[{:d}] \t {:12.2f} \t {:12.2f}".format(i, coeffs_i[i], coeffs_ii[i]))

# %%
p_i = [exercise.horner(Delta_i, coeffs_i, x) for x in x_cont]
p_ii = [exercise.horner(Delta_ii, coeffs_ii, x) for x in x_cont]

pn_i = [exercise.f(x) for x in Delta_i]
pn_ii = [exercise.f(x) for x in Delta_ii]

# %%
plt.rcParams["figure.dpi"] = 120

plt.plot(x_cont, f_cont, "--k", label="f(x)")
plt.plot(x_cont, p_i, "r", label="p(x)")
plt.plot(Delta_i, pn_i, "or", label="support nodes (i)")
plt.plot(x_cont, p_ii, "b", label="p(x)")
plt.plot(Delta_ii, pn_ii, "ob", label="support nodes (ii)")
plt.title("Polynomial interpolation")
plt.legend()
plt.xlabel("x")
plt.ylabel("y")
plt.xlim([-1.0, 1.0])
plt.ylim([-2.5, 5.5])
plt.show()


# %% [markdown]
# Calculate the interpolating polynomial for $\Delta_{12}$
#
# We can now repeat the same calculation for 12 and 22 points.

# %%
Delta_i = exercise.create_interval_i(12)
Delta_ii = exercise.create_interval_ii(12)

# %%
coeffs_i = exercise.interpolate(exercise.f, Delta_i)
coeffs_ii = exercise.interpolate(exercise.f, Delta_ii)

# %% [markdown]
# The coefficients are:

# %%
for i in range(0, len(Delta_i)):
    print("\t a[{:d}] \t {:12.2f} \t {:12.2f}".format(i, coeffs_i[i], coeffs_ii[i]))

# %%
p_i = [exercise.horner(Delta_i, coeffs_i, x) for x in x_cont]
p_ii = [exercise.horner(Delta_ii, coeffs_ii, x) for x in x_cont]

pn_i = [exercise.f(x) for x in Delta_i]
pn_ii = [exercise.f(x) for x in Delta_ii]

# %%
plt.plot(x_cont, f_cont, "--k", label="f(x)")
plt.plot(x_cont, p_i, "r", label="p(x)")
plt.plot(Delta_i, pn_i, "or", label="support nodes (i)")
plt.plot(x_cont, p_ii, "b", label="p(x)")
plt.plot(Delta_ii, pn_ii, "ob", label="support nodes (ii)")
plt.title("Polynomial interpolation")
plt.legend()
plt.xlabel("x")
plt.ylabel("y")
plt.xlim([-1.0, 1.0])
plt.ylim([-2.5, 5.5])
plt.show()

# %% [markdown]
# Calculate the interpolating polynomial for $\Delta_{22}$

# %%
Delta_i = exercise.create_interval_i(22)
Delta_ii = exercise.create_interval_ii(22)

# %%
coeffs_i = exercise.interpolate(exercise.f, Delta_i)
coeffs_ii = exercise.interpolate(exercise.f, Delta_ii)

# %% [markdown]
# The coefficients are:

# %%
for i in range(0, len(Delta_i)):
    print("\t a[{:d}] \t {:12.2f} \t {:12.2f}".format(i, coeffs_i[i], coeffs_ii[i]))

# %%
p_i = [exercise.horner(Delta_i, coeffs_i, x) for x in x_cont]
p_ii = [exercise.horner(Delta_ii, coeffs_ii, x) for x in x_cont]

pn_i = [exercise.f(x) for x in Delta_i]
pn_ii = [exercise.f(x) for x in Delta_ii]
# %%

plt.plot(x_cont, f_cont, "--k", label="f(x)")
plt.plot(x_cont, p_i, "r", label="p(x)")
plt.plot(Delta_i, pn_i, "or", label="support nodes (i)")
plt.plot(x_cont, p_ii, "b", label="p(x)")
plt.plot(Delta_ii, pn_ii, "ob", label="support nodes (ii)")
plt.title("Polynomial interpolation")
plt.legend()
plt.xlabel("x")
plt.ylabel("y")
plt.xlim([-1.0, 1.0])
plt.ylim([-2.5, 5.5])
plt.show()
