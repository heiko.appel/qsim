#!/usr/bin/env python3
# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:hydrogen
#     text_representation:
#       extension: .py
#       format_name: hydrogen
#       format_version: '1.3'
#       jupytext_version: 1.10.3
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# %% [markdown]
# Copyright (c) 2004, 2021, Heiko Appel
# Copyright (c) 2021, Nicolas Tancogne-Dejean
# Copyright (c) 2021, Martin Lueders
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.

# %% [markdown]
# # Numerical Accuracy (2)
#
# ##  Numerical cancellation
#
# In both expressions, there can be situations, in which we have to take
# differences of large numbers or divisions ...
#
# Part 1:
#
# When calculating
#
# \begin{eqnarray}
# \label{fx}
# f(x) & = & \underbrace{\frac{1}{1+2x}}_{\approx 1} - \underbrace{\frac{1-x}{1+x}}_{\approx 1}
# \qquad \mbox{for}\,\, |x|\ll 1
# \end{eqnarray}
#
# with floating point arithmetics is numerically problematic, as for arguments
# $|x|\ll 1$ both terms are approximately 1 which therefore leads during the
# subtraction to a cancellation of significant digits.  A better conditioned
# expression for $f(x)$ can be obtained by performing some equivalence
# transformations:
#
# \begin{eqnarray}
# \label{umformung1}
# f(x) & = & \frac{1}{1+2x} - \frac{1-x}{1+x} = \frac{1+x-(1-x)(1+2x)}{(1+2x)(1+x)} \nonumber \\
#      & = & \frac{1+x-1-2x+x-2x^2}{1+x+2x+2x^2} = \frac{2x^2}{2x^2+3x+1}.
# \end{eqnarray}(form1)
#
# The resulting form is, in comparison to the original, better suited for
# numerical evaluation.  For the case  $x \geq 0$ we now only have numerically
# stable operations, such as multiplication, division und summation of terms
# with equal sign.  For $x< 0$, the contribution $3x$ in the denominator is
# always negative. There is, however, no cancellation of significant digits, as
# the terms of the denominator are of different order of magnitude.
#

# %%
import numpy as np
import matplotlib.pyplot as plt
from qsim.utils import display_source

import qsim.numerical_accuracy_2_master as exercise

# %%
display_source(exercise.f1)

# %%
display_source(exercise.f2)

# %%
x_values = np.arange(-1e-7, 1e-7, 1e-10)
f1_values = [exercise.f1(x) for x in x_values]
f2_values = [exercise.f2(x) for x in x_values]

# %%
plt.rcParams["figure.dpi"] = 120
plt.plot(x_values, f1_values, label="original")
plt.plot(x_values, f2_values, label="reformulated")
plt.xlabel("x")
plt.ylabel("f(x)")
plt.legend()
plt.show()

# %% [markdown]
# Part 2:
#
# Also for $g(x)$ the given form
#
# \begin{eqnarray}
# g(x) & = & \frac{\overbrace{1}^{=1}-\overbrace{\cos x}^{\approx 1}}{x}
# \qquad \mbox{for}\,\, |x|\ll 1
# \end{eqnarray}
#
# is unfavourable for numerical evaluation.
#
# For $|x|\ll 1$ we get $\cos(x) \approx 1$ so that we can have a cancellation
# of significant digits in the numerator.  This expression can be improved by
# using trigonometric identities:
#
# \begin{eqnarray}
# g(x) & = & \frac{1-\cos x}{x} = \frac{\frac{1}{2} (1-\cos x)}{\frac{1}{2} x}
#        =   \frac{\sin^2 \frac{x}{2}}{\frac{1}{2} x} \nonumber \\
#      & = & \frac{2 \sin^2 \frac{x}{2}}{x}.
# \end{eqnarray}
#
# The resulting expression only requires multiplications and divisions, which
# are numerically stable.
#
# %%
display_source(exercise.g1)

# %%
display_source(exercise.g2)

# %%
x_values = np.arange(-1e-7, 1e-7, 1e-10)
g1_values = [exercise.g1(x) for x in x_values]
g2_values = [exercise.g2(x) for x in x_values]

# %%
plt.plot(x_values, g1_values, label="original")
plt.plot(x_values, g2_values, label="reformulated")
plt.xlabel("x")
plt.ylabel("g(x)")
plt.legend()
plt.show()

# %% [markdown]
#
# ## Numerical derivatives
#
# When calculating the given difference quotients - as in actually all
# numerical algorithms - one has to distinguish two sources of errors: \
# (i) Rounding errors, or errors due to numerical cancellation \
# (ii) Errors due to the approximate formula, i.e. truncation errors. \
# Rounding errors occur at all floating point operations and depend on the
# machine precision.  Truncation errors are determined by the order of the used
# approximation, and also would occur if exact arithmetics was used.
#
# **Forward difference quotient**
#
# From the Taylor expansion of the function $f(x)$,
#
# $$
#  f(x+h) = f(x) + f'(x)\,h + \frac{1}{2}\,f''(x)\,h^2 +\frac{1}{6}\,f'''(x)\,h^3 +  O(h^4) ,
# $$
#
# and the definition of the absolute error, we obtain:
#
# $$
#  \Delta^{(a)}(x,h) &=& \left|f'(x) - \frac{f(x) + f'(x)\,h + 1/2\, f''(x)\,h^2 + 1/6\, f'''(x)\,h^3
#      + ...  - f(x)}{h}\right|  \nonumber \\
#              &=& \left| \frac{1}{2} \,f''(x)\,h + \frac{1}{6} \,f'''(x)\,h^2
#                + O(h^3) \right|
# $$ (deltaxha)
#
# 1. For $f(x)=x^3$, $f'(x)=3x^2$, $f''(x)=6x$, $f'''(x)=6$ we get from {eq}`deltaxha` with $f \geq 0$
#    \begin{eqnarray}
#    \label{ai}
#      \Delta^{(a)}(x=1,h) &=& \frac{1}{2} 6\times 1 h + \frac{1}{6} 6 h^2  + O(h^3) \nonumber \\
#                    &=& 3 h + h^2 + O(h^3).
#    \end{eqnarray}
#    For values $0\leq h\ll 1$ the linear term is dominant, i.e. the error $\Delta(x,h)$ as a function of $h$ is linear
#    in the vicinity of $x=1$.
#
#
# 2. An analogous behaviour can be found for $h\geq0$, $f(x)=e^x$, $f^{(i)}(x)=e^x$, $i=1,2,...$ when inserting this
# into {eq}`deltaxha`.
#
# \begin{eqnarray}
# \label{aii}
#   \Delta^{(a)}(x=1,h) &=& \frac{1}{2} \,e^1\,h + \frac{1}{6} \,e^1\,h^2  + O(h^3). \nonumber
# \end{eqnarray}
#
# We will now calculate the errors of the difference quotients and display them
# on a double-logarithmic scale.

# %% [markdown]
#
# All error-curves show a similar and typical behaviour:
#
# - Starting at $h=1$, the error follows the linear shape, which was discussed
#   above. Here the errors due to the discretization are dominant and roundoff
#   errors are not significant.
#
# - For $h < 10^{-8}$ the errors start to increase again. The reason for this
#   are now the roundoff errors, which occur in the calculation of the difference
#   quotients. For the error analysis we set
#     \begin{eqnarray}
#     f(x+h) &=& \mbox{rd}(f(x+h)) + e_{x+h} \\
#     f(x)   &=& \mbox{rd}(f(x)) + e_x \\
#     f'(x)   &=& \mbox{rd}(f'(x)) + r_x \\
#     d(x,h) &=& \frac{f(x+h)-f(x)}{h}
#     \end{eqnarray}
#   where $e_{x+h}$ and $e_{x}$ denote the differences between the exact
#   function values and the values in floating point arithmetics
#   $\mbox{rd}(\ldots)$. Analogously, we define $r_{x}$ for the derivative.
#   Hence, for a calculation of $d(x,h)$ in floating point arithmetic we get
#
#   \begin{eqnarray}
#   \mbox{rd}(d(x,h)) \approx d(x,h) + \frac{e_{x}-e_{x+h}}{h},
#   \end{eqnarray}
#   where the rounding errors during the division by $h$ can be neglected
#   (indicated by the symbol $\approx$).  The floating point form of the
#   absolute error $\mbox{rd}(\Delta^{(a)}(x,h))$ results in:
#
#     $$
#      \label{rundung}
#       \mbox{rd}( \Delta^{(a)}(x,h) ) &=& \left|\mbox{rd}(f'(x)) - \mbox{rd}\left(\frac{f(x+h) - f(x)}{h}\right)\right| \nonumber \\
#                      &\approx& \left|f'(x) - r_x - \frac{\mbox{rd}(f(x+h)) - \mbox{rd}(f(x))}{h}\right| \nonumber \\
#                      &=& \left|f'(x) - \frac{f(x+h) - f(x)}{h} + \frac{e_{x}-e_{x+h}}{h} - r_x \right|
#     $$ (rundung)
#
#   where we have assumed that the rounding and taking the absolute value
#   commute, and we can again neglect rounding errors in the division in the
#   second line.
#
#   The first two terms in {eq}`rundung` are evaluated in analogy to
#   {eq}`deltaxha` using a Taylor expansion
#
#    $$
#     \label{updown}
#      \mbox{rd}( \Delta^{(a)}(x,h) ) \approx \left| \frac{1}{2} \,f''(x)\,h + \frac{1}{6} \,f'''(x)\,h^2
#                     + \ldots + \frac{e_{x}-e_{x+h}}{h} - r_x  + O(h^3) \right|.
#    $$ (updown)
#
#   The result shows, that for small values of $h$ initially the linear term
#   $f''(x)\,h$ dominates.  The value of the contributions due to rounding
#   $e_{x},e_{x+h}$ determines then the range in which the inverse term
#   $(e_{x}-e_{x+h})/h$ takes the leading role. As we neglected rounding errors
#   in the division at twice in the above argumentation, the precise behaviour
#   of the error can deviate from $1/h$ in a practical calculation.
#
# - Once $h$ reaches the order of the machine accuracy, $2.22\times10^{-16}$,
#   one observes a constant behaviour of the absolute error.  The reason for this
#   is the rounding error in the calculation of the difference $f(x+h)-f(x)$.  If
#   $h < $`eps`,  then $f(x+h)-f(x)=0$ and the expression for the absolute error
#   reduces to
#
#   \begin{eqnarray}
#     \Delta^{(a)}(x,h) &=& \left|f'(x) - \frac{f(x+h) - f(x)}{h}\right| =
#     \left|f'(x) - \frac{0}{h}\right| = \left|f'(x)\right|
#   \end{eqnarray}
#   i.e. the function $\Delta^{(a)}(x,h)$ is independent of $h$. The constant
#   behaviour of $\Delta^{(a)}(1,h)=|f'(1)|$ is clearly visible in all graphs
#   for $h<2.22\times10^{-16}$.

# %% [markdown]
#
# Create arrays containing the nodes, corresponding to (i) and (ii):

# %%
h_2 = [pow(2, -x) for x in range(0, 80)]
h_10 = [pow(10, -x) for x in range(0, 25)]

# %% [markdown]
# Now, create arrays containing the $\Delta_{(i/ii)}(1,h)$ for the two example
# functions.

# %%
delta_2_f_i = [
    abs(exercise.fprime_i(1.0) - exercise.diffquot_f(exercise.f_i, 1.0, h)) for h in h_2
]
delta_2_f_ii = [
    abs(exercise.fprime_ii(1.0) - exercise.diffquot_f(exercise.f_ii, 1.0, h))
    for h in h_2
]

delta_10_f_i = [
    abs(exercise.fprime_i(1.0) - exercise.diffquot_f(exercise.f_i, 1.0, h))
    for h in h_10
]
delta_10_f_ii = [
    abs(exercise.fprime_ii(1.0) - exercise.diffquot_f(exercise.f_ii, 1.0, h))
    for h in h_10
]

# %% [markdown]
# Comparison of different functions for nodes of the form $h_j=2^j$

# %%
plt.xscale("log")
plt.yscale("log")
plt.xlabel("h")
plt.ylabel("$\Delta$")
plt.plot(h_2, delta_2_f_i, "-o", label="f(x)=x^3")
plt.plot(h_2, delta_2_f_ii, "-+", label="f(x)=e^x")
plt.title("Comparison for nodes $h_j=2^(-j)$")
plt.legend()
plt.show()

# %% [markdown]
# Comparison of different functions for nodes of the form $h_j=10^j$

# %%
plt.xscale("log")
plt.yscale("log")
plt.xlabel("h")
plt.ylabel("$\Delta$")
plt.plot(h_10, delta_10_f_i, "-o", label="f(x)=x^3")
plt.plot(h_10, delta_10_f_ii, "-+", label="f(x)=e^x")
plt.title("Comparison for nodes $h_j=10^(-j)$")
plt.legend()
plt.show()

# %% [markdown]
# Comparison of different nodes for $f(x)=x^3$

# %%
plt.xscale("log")
plt.yscale("log")
plt.xlabel("h")
plt.ylabel("$\Delta$")
plt.plot(h_2, delta_2_f_i, "-o", label="h_j=2^(-j)")
plt.plot(h_10, delta_10_f_i, "-+", label="h_j=10^(-j)")
plt.title("Comparison for f(x)=x^3")
plt.legend()
plt.show()

# %% [markdown]
# Comparison of different nodes for $f(x)=e^x$

# %%
plt.xscale("log")
plt.yscale("log")
plt.xlabel("h")
plt.ylabel("$\Delta$")
plt.plot(h_2, delta_2_f_ii, "-o", label="h_j=2^(-j)")
plt.plot(h_10, delta_10_f_ii, "-+", label="h_j=10^(-j)")
plt.title("Comparison for f(x)=e^x")
plt.legend()
plt.show()

# %% [markdown]
#
# When calculating numerical derivatives, it is advisable that $h$ is chosen to
# be in first range, where the error follows the Taylor expansion, and rounding
# errors do not play a significant role.
#
# A special case is $f(x)=x^3$ when the abscissae are of the form $h_j=2^{-j}$,
# j=0,..,17.  All values, which are necessary to calculate the derivatives in
# this case are exactly representable as machine numbers (only powers of 2 are
# involved). Therefore, rounding errors $e_{x},e_{x+h}$ vanish exactly.  Only
# starting at $j=18$, the floating point representation of $f(x+h)-f(x)$, due
# to rounding, only the most significant bit in mantissa0 is set.
# ```
# appel@g35:~> float2bits_Linux 1.11758708953857421875e-08
# ...
# _double______________________________________________________________________
# binary..: s|--exponent-|------mantissa0-----|------------mantissa1-----------
#         : s|09876543210|10987654321098765432|10987654321098765432109876543210
#         : 0|01111100100|10000000000000000000|00000000000000000000000000000000
# negative: 0
# exponent: 996 [0x000003e4]
# mantiss0:     524288 [0x00080000]      524288/(2^20)=0.500000
# mantiss1:          0 [0x00000000]           0/(2^32)=0.000000
# together: (+1)*((524288+0/2^32)/2^20+1.)*2^(996-1023) =
#           0.00000001117587089539 = 1.11758708953857421875e-08
# ```
#
# Further reduction of $h$ only changes the exponent. Here is an example for $j=46$:
# ```
# appel@g35:~> float2bits_Linux 4.26325641456060111523e-14
# ...
# _double______________________________________________________________________
# binary..: s|--exponent-|------mantissa0-----|------------mantissa1-----------
#         : s|09876543210|10987654321098765432|10987654321098765432109876543210
#         : 0|01111010010|10000000000000000000|00000000000000000000000000000000
# negative: 0
# exponent: 978 [0x000003d2]
# mantiss0:     524288 [0x00080000]      524288/(2^20)=0.500000
# mantiss1:          0 [0x00000000]           0/(2^32)=0.000000
# together: (+1)*((524288+0/2^32)/2^20+1.)*2^(978-1023) =
#           0.00000000000004263256 = 4.26325641456060111523e-14
# ```
#
# A subsequent division of $f(x+h)-f(x)$ by $h_j=2^{-j}$, or equivalently a
# multiplication with $h_j=2^{j}$ can also be calculated exactly in floating
# point arithmetic. This only requires manipulations of the exponent.
# \begin{eqnarray}
# j=28: && \qquad (+1)\times((524288 + 0/2^{32})/2^{20} + 1)\times2^{(996 - 1023)}\times2^{28} = 3 \nonumber \\
# j=46: && \qquad (+1)\times((524288 + 0/2^{32})/2^{20} + 1)\times2^{(978 - 1023)}\times2^{46} = 3 \nonumber
# \end{eqnarray}
#
# The rounding, which is necessary for the subtraction $f(x+h)-f(x)$ in the
# range $h<10^{-8}$ leads therefore to the exact value of the derivative. The
# absolute error only jumps to the value of $f'(1)$, when $h$ becomes smaller
# than `eps`.
#
# For the exponential function we have $e_{x}\neq0,e_{x+h}\neq0$ (the exact
# value of the exponential has to be rounded to a machine number at all values
# of the abscissa).  Special forms of the mantissa, which appeared for
# $f(x)=x^3$ in the difference $f(x+h)-f(x)$, do not occur here, i.e.  for the
# exponential function we expect again the normal {eq}`updown` behaviour of the
# errors.
#

# %% [markdown]
# **Central difference quotient**
#
# In case of the central difference quotient the Taylor expansion
# \begin{equation}
# f(x\pm h) = f(x) \pm f'(x)\,h + \frac{1}{2}\,f''(x)\,h^2 \pm \frac{1}{6}\,f'''(x)\,h^3 +  O(h^4)
# \end{equation}
# gives rise to the error:
# \begin{eqnarray}
# \label{deltaxh}
#   \Delta^{(b)}(x,h) &=& \left|f'(x) - \frac{f(x) + f'(x)\,h + 1/2\, f''(x)\,h^2 + ...
#       - f(x) + f'(x)\,h - 1/2\, f''(x)\,h^2 +  ...}{2h}\right|  \nonumber \\
#               &=& \left|\frac{1}{6}\,f'''(x)\,h^2 + \frac{1}{120}\,f^{(5)}\, h^4 + O(h^6)\right|
# \end{eqnarray}
#
# 1. For $f(x)=x^3$ with $h \geq 0$ we get
#    \begin{eqnarray}
#     \Delta^{(b)}(x=1,h) &=& \frac{1}{6}\,6\,h^2 \,\,=\,\, h^2
#    \end{eqnarray}
# 2. Analogously, for $f(x)=e^x$ with $h \geq 0$, one obtains
#    \begin{eqnarray}
#      \Delta^{(b)}(x=1,h) &=& \frac{1}{6}\,e^1\,h^2 + \frac{1}{120}\,e^1\, h^4 + O(h^6)
#    \end{eqnarray}
#    For values $0\leq h\ll 1$, again, the only lowest term of the expansion is
#    dominant.  In a double-logarithmic plot, the quadratic shape of the error
#    $\Delta^{(b)}(x=1,h)$ as function of $h$ appears linear. Comparison of the
#    errors for the two difference quotient methods shows, that for values of
#    $h$ in the range $[10^{-5},10^{0}]$, the central difference quotient
#    yields more accurate results. However, rounding errors start affecting the
#    results earlier than in the forward difference quotient.  The errors are
#    nevertheless always below those of the forward difference quotient.

# %%
delta_2_c_i = [
    abs(exercise.fprime_i(1.0) - exercise.diffquot_c(exercise.f_i, 1.0, h)) for h in h_2
]
delta_2_c_ii = [
    abs(exercise.fprime_ii(1.0) - exercise.diffquot_c(exercise.f_ii, 1.0, h))
    for h in h_2
]

delta_10_c_i = [
    abs(exercise.fprime_i(1.0) - exercise.diffquot_c(exercise.f_i, 1.0, h))
    for h in h_10
]
delta_10_c_ii = [
    abs(exercise.fprime_ii(1.0) - exercise.diffquot_c(exercise.f_ii, 1.0, h))
    for h in h_10
]

# %% [markdown]
# Comparison of different functions for nodes of the form $h_j=2^j$

# %%
plt.xscale("log")
plt.yscale("log")
plt.xlabel("h")
plt.ylabel("$\Delta$")
plt.plot(h_2, delta_2_c_i, "-o", label="f(x)=x^3")
plt.plot(h_2, delta_2_c_ii, "-+", label="f(x)=e^x")
plt.title("Comparison for nodes $h_j=2^(-j)$")
plt.legend()
plt.show()

# %% [markdown]
# Comparison of different functions for nodes of the form $h_j=10^j$

# %%
plt.xscale("log")
plt.yscale("log")
plt.xlabel("h")
plt.ylabel("$\Delta$")
plt.plot(h_10, delta_10_c_i, "-o", label="f(x)=x^3")
plt.plot(h_10, delta_10_c_ii, "-+", label="f(x)=e^x")
plt.title("Comparison for nodes h_j=10^(-j)")
plt.legend()
plt.show()

# %% [markdown]
# Comparison of different nodes for $f(x)=x^3$

# %%
plt.xscale("log")
plt.yscale("log")
plt.xlabel("h")
plt.ylabel("$\Delta$")
plt.plot(h_2, delta_2_c_i, "-o", label="h_j=2^(-j)")
plt.plot(h_10, delta_10_c_i, "-+", label="h_j=10^(-j)")
plt.title("Comparison for f(x)=x^3")
plt.legend()
plt.show()

# %% [markdown]
# Comparison of different nodes for $f(x)=e^x$

# %%
plt.xscale("log")
plt.yscale("log")
plt.xlabel("h")
plt.ylabel("$\Delta$")
plt.plot(h_2, delta_2_c_ii, "-o", label="h_j=2^(-j)")
plt.plot(h_10, delta_10_c_ii, "-+", label="h_j=10^(-j)")
plt.title("Comparison for f(x)=e^x")
plt.legend()
plt.show()

# %% [markdown]
# Comparison of the difference quotients: $f(x)=x^3$ and nodes $h_j=2^j$

# %%
plt.xscale("log")
plt.yscale("log")
plt.xlabel("h")
plt.ylabel("$\Delta$")
plt.plot(h_2, delta_2_f_i, "-o", label="forward")
plt.plot(h_2, delta_2_c_i, "-+", label="central")
plt.title("Comparison of the Difference quotients \n f(x)=x^3 - nodes h_j=2^j")
plt.legend()
plt.show()

# %% [markdown]
# Comparison of the difference quotients: $f(x)=x^3$ and nodes $h_j=10^j$

# %%
plt.xscale("log")
plt.yscale("log")
plt.xlabel("h")
plt.ylabel("$\Delta$")
plt.plot(h_10, delta_10_f_i, "-o", label="forward")
plt.plot(h_10, delta_10_c_i, "-+", label="central")
plt.title("Comparison of the Difference quotients \n f(x)=x^3 - nodes h_j=10^j")
plt.legend()
plt.show()

# %% [markdown]
# Comparison of the difference quotients: $f(x)=e^x$ and nodes $h_j=2^j$

# %%
plt.xscale("log")
plt.yscale("log")
plt.xlabel("h")
plt.ylabel("$\Delta$")
plt.plot(h_2, delta_2_f_ii, "-o", label="forward")
plt.plot(h_2, delta_2_c_ii, "-+", label="central")
plt.title("Comparison of the Difference quotients \n f(x)=e^x - nodes h_j=2^j")
plt.legend()
plt.show()

# %% [markdown]
# Comparison of the difference quotients: $f(x)=e^x$ and nodes $h_j=10^j$

# %%
plt.xscale("log")
plt.yscale("log")
plt.xlabel("h")
plt.ylabel("$\Delta$")
plt.plot(h_10, delta_10_f_ii, "-o", label="forward")
plt.plot(h_10, delta_10_c_ii, "-+", label="central")
plt.title("Comparison of the Difference quotients \n f(x)=e^x - nodes h_j=10^j")
plt.legend()
plt.show()
