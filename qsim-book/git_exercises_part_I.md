# Git Exercises, part I

The git revision control system has become the de-facto industry standard for
code revision control.

For the purpose of the present course, we only cover very basic and elementary
git commands. You can learn more advanced handling of git in the upcoming
Advanced Octopus course, where topics like git rebase, git filter, etc. will be
covered.


## Git identity

Before we start, we need to tell git which username and e-mail we would like to
use for our git commits. This can be achieved with

```shell
[git-01] > git config --global user.name "myFirstname myLastname"
[git-02] > git config --global user.email "myMailname@example.com"
```
where you should replace of course "myFirstname myLastname" and
"myMailname@example.com" by your actual name and e-mail.


## Initialize git repository

To create a git repository, open a shell and execute the following commands
```shell
[git-03] > git init first_git_repo
[git-04] > cd first_git_repo
```

## Create new file
```shell
[git-05] > echo "Hello World!" > hello.txt
```

## Show status
```shell
[git-06] > git status
```

## Add file to the git repository
```shell
[git-07] > git add hello.txt
```

## Show status
```shell
[git-08] > git status
```

## Commit added files
```shell
[git-09] > git commit -m 'Initial commit.'
```

## Show status
```shell
[git-10] > git status
```

## Listing all checked in files in your repository
```shell
[git-11] > git ls-files
```

## Add more files to the repository
```shell
[git-12] > for x in $(seq 1 3); do echo $x > $x.txt; git add $x.txt; git commit $x.txt -m "commit #$x"; done
```
Instead of the loop above, you can of course perform all steps manually.

## Create new feature branch
```shell
[git-13] > git checkout -b feature_branch
```

## Add some commits to the feature branch
```shell
[git-14] > for x in $(seq 4 6); do echo $x > $x.txt; git add $x.txt; git commit $x.txt -m "commit #$x"; done
```

## Switch back to master branch
```shell
[git-15] > git checkout master
```

## Merge feature branch into master branch
```shell
[git-16] > git merge feature_branch
```

## Inspect the history of your repository
```shell
[git-17] > git log --graph --pretty
```

## Git browser: git cola, tig, gitk, fugitive

There are many utilities available to browse and inspect git repositories.
Examples include git cola, tig, gitk, and fugitive. Many more can be found under
e.g.

[https://www.slant.co/options/16696/alternatives/~gitk-alternatives](https://www.slant.co/options/16696/alternatives/~gitk-alternatives)

Install one of these tools on your computer and inspect your repository.


## Accidentally deleting a file ...
```shell
[git-18] > rm hello.txt
```

## ... show status ...
```shell
[git-19] > git status
```

## ... and getting it back from the git archive of your repository
```shell
[git-20] > git checkout hello.txt
```

## Create again a new feature branch
```shell
[git-21] > git checkout -b my_hotfix
```

## List all branches
```shell
[git-22] > git branch -avv
```

## Modify hello.txt
```shell
[git-23] > echo "Hi World!" > hello.txt
```
## Add and commit your changes
```shell
[git-24] > git add hello.txt
[git-25] > git commit hello.txt -m "small fix"
```

## Switch back to master branch
```shell
[git-26] > git checkout master
```

## Merge feature branch into master branch
Note, this will create a merge conflict
```shell
[git-27] > git merge my_hotfix
```
Resolve the conflict by changing the text in hello.txt
and by removing the merge conflict markers. Then add
the file to the staging area, and commit.
```shell
[git-28] > git add hello.txt
[git-29] > git commit hello.txt
```
Congratulations! You resolved your first merge conflict.




:::{admonition} Exercises part I
:class: seealso
Run commands [git-01]..[git-29] in your shell and make yourself familiar with these commands.
:::
