# Numerical Accuracy (2)

## Numerical cancellation

Many expressions are ill suited for numerical evaluation. Reformulate the
following expressions to make them numerically stable:

1)  $$ f(x) = \frac{1}{1 + 2x} - \frac{1-x}{1+x} \qquad \mbox{for} \qquad |x| \ll 1 . $$
2)  $$ g(x) = \frac{1-\cos(x)}{x} \qquad \mbox{for} \qquad x \ne 0 , |x| \ll 1 . $$

Explain also why the original expressions are problematic, and investigate the
original and reformulated expressions numerically.

## Numerical derivation

In analysis, the derivative of a function is usually defined by:

$$ f'(x) := \lim_{h\to0} \frac{f(x+h)-f(x)}{h} $$

Obviously, the involved limiting process is not practical on a computer using
floating point arithmetics.  In order to investigate the effect of rounding
errors, consider the difference quotient

$$ \Delta(x,h) := \left|f'(x) - \frac{f(x+h)-f(x)}{h}\right| $$

as a function of $h$.

1) Plot the error $\Delta(1,h)$ for the functions
    (i)  $f(x) = x^3$ and
    (ii) $f(x) = e^x$
    Use for the abscissa the values $h_j =10^{-j}$ for $1 \le j \le 25$. \
    What do you observe, if you use the abscissae $h_j = 2^{-j}$ for $1 \le j \le 80$? \
    How do you explain the difference?

2) Replace the difference quotient, defined above, by

$$ f'(x) \approx \frac{f(x+h)-f(x-h)}{2h} $$

Compare the errors, resulting from this expression to the ones from part (1).
What do you observe?
