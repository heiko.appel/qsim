# FFT: Restoring damaged Electro-Cardiogram (ECG) data.


```{image} ecg.png
:alt: ECD data
:align: center
```

The above graph displays a recording of an aged ECG machine. You can find the
data in the file <a href="ecg.dat">ecg.dat</a>. The file contains a list of
4096 discrete voltages (in arbitrary units), which were recorded every 2 ms.

Create first a plot as given above (voltages against time). Then, determine the
Fourier coefficients and plot their amplitude. Besides the (fast decaying)
peaks at low frequency, you should notice a further peek at a higher frequency.
What is this frequency in Hz, and what could be the origin of that component of
the signal?

Try to repair the damage by eliminating the unwanted frequency (and possibly
also higher frequencies) from the spectrum, and transform back to the time
domain.

Note that the corrected Fourier coefficients still need to comply with the
symmetry  $b_s^{*} = b_{n-s}$.

