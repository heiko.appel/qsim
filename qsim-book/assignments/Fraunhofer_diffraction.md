# Fraunhofer diffraction

```{image} diffraction.png
:alt: Fraunhofer diffraction
:align: center
```

Consider the Fraunhofer diffraction at a hole in an infinitely extended plane
$\alpha$.  The intensity profile $I(\mathbf{P})$ at some point $\mathbf{P} =
(p,q)$ in a second plane $\beta$, which is parallel to $\alpha$ at some
distance behind the hole can be described by the Fraunhofer diffraction
integral

$$
  U(\mathbf{P}) &=& U(p,q) = \frac{1}{\lambda R} \sqrt{\frac{\cal P}{D}} \,
  \int\int G(\xi,\eta) \exp
  \left( -i\frac{2\pi}{\lambda}(p\xi+q\eta) \right) \, d\xi \, d\eta, \\
  I(\mathbf{P}) &=& |U(\mathbf{P})|^2.
$$

For simplicity, we set $\lambda=1$ and $1/(R\sqrt{D/{\cal P}})=1$.  The
intensity profile in the plane $\beta$ behind the hole is therefore given by a
two-dimensional Fourier transform of the shape of the opening.

In the corresponding discretized form the integral can be calculated by a
two-dimensional FFT.

Use the numpy function `fft.fft2()` and calculate the intensity profile in the
plane $\beta$ for an opening of the shape:

$$
G(\xi,\eta) = \left\{ \begin{array}{ll} 1 &
               \qquad\mbox{for $-c\leq \xi\leq c$ and $-c\leq \eta\leq c$,}  \\
                                    0     &
               \qquad\mbox{otherwise} \end{array} \right.
$$

Choose for displaying the plane a grid of $1024\times 1024$ points with the
centre of the lattice being at the origin, and determine $c$ such that $2c$
corresponds to about 18 grid points. What do you observe when you increase or
decrease $c$ and hence the area of the opening?

Also compute the diffraction patterns for an opening with a shape of your
choice. Discuss the resulting diffraction patterns.
