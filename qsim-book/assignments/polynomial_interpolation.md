# Polynomial Interpolation

For the Newtonian interpolation problem of the function

$$ f(x) = \frac{5}{1+25x^2}, \qquad x \in [a,b], \quad a=-1,\,b=1,$$

the interval divisions $\Delta_n =\{a\leq x_0^{(n)} < x_1^{(n)} <...< x_{n}^{(n)}\leq b\}$ with $n$ being a natural number are given:

(i) $x_i^{(n)} = a + ih, \qquad$ with $\qquad h=\frac{b-a}{n}, \qquad$ for $\quad i=0,...,n$

(ii) $x_i^{(n)} = -\cos \left( \frac{2i+1}{n+1} \frac{\pi}{2} \right) \qquad$ for $\quad i=0,...,n$

## Part 1) 

Calculate (without computer) the nodes of the interval division $\Delta_4$ for the form (i) the table of the divided differences for the function $f(x)$ and give the Newton form of the interpolating polynomial.

## Part 2) 

Write a Python code using the formalism of divided differences to generate the coefficients $a_0, a_1, ..., a_n$ of the Newton form of he interpolating polynomial $P_n(x)$. Use the following algorithm:
- for each $i = 0, 1, ..., n$
  - $t_i = f_i$
  - for $j=i-1, i-2, ..., 0 \quad$ where $\quad i \ge 1$
    - $t_j = (t_{j+1} - t_j)/(x_i - x_j)$
  - $a_i = t_0$

where $f_i$ are the function values at the nodes $x_i$. The evaluation of the polynomial at an arbitrary $x \in [a, b]$ can be done with the following Horner-like scheme:
- $p = a_n$
- for $i=n-1, n-2, ..., 0$
  - $p = p (x - x_i) + a_i$

Calculate, using the nodes of form (i) for the interval divisions $\Delta_n$ fpr $n=5,12,22$, the value $p$ of the interpolating polynomial at $x = \pi/4$ and determin the corresponding error $|f(x) - P_n(x)|$.

## Part 3) 

Calculate the interpolating polynomial $P_n(x)$, $n= 5, 12, 22$, for the nodes of the forms (i) and (ii) and evaluate the the polynomials at the arguments  $x_i^{(k)}$, $i=0,....,k$ of the form (i) with $k=500$. Plot the function $f(x)$ and the interpolating polynomials $P_n(x)$.
