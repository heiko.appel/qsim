# Electrostatic potential

```{image} charge.png
:alt: Fraunhofer diffraction
:align: center
```

Consider the x-y plane with the following regions: $M$,, $G_j$ for $j=1,2$

$$
  G_j = \left\{\left[\begin{array}{c} x \\ y \end{array} \right] | 
               -a_j \leq x,y \leq a_j \right\},
  \qquad \mbox{with}  \quad a_1=1, \, a_2=5, \quad  M = G_2\backslash G_1.
$$

Region $G_1$ contains a homogeneous charge distribution $\rho$. For points
$(x_0, y_0)$ in the region $M$, the electrostatic potential $\Phi(x_0, y_0)$ is
given by the following integral:

$$
  \Phi(x_0,y_0) = \frac{\rho}{4\pi\epsilon_0} \int_{-1}^{1}\int_{-1}^{1}\,
                    \frac{1}{\sqrt{(x-x_0)^2+(y-y_0)^2}}\,dx\,dy.
$$ (potential)


## Gauss-Legendre integration

Calculate the two-dimensional integral in {eq}`potential` using a
Gauss-Legendre quadrature with 10 support points. For simplicity, set
$\rho/4\pi\epsilon_0=1$.

Hint: you can obtain the Gauss-Legendre points and weights using the numpy
function: ``` nodes, weights = np.polynomial.legendre.leggauss(n) ```

Check your code by comparing to the exact result for the point $(2,2)$:

$$
        \Phi(2,2) = 8\,\mbox{arcsinh}(1)-2\,\mbox{arcsinh}(3)+
                     3\ln(-1 + \sqrt{10}) - 3\ln(1 +\sqrt{10})
$$


Now calculate $\Phi(x_0,y_0)$ for the points of the form $x_{i,j} = (-a_2+i
h,-a_2+j h)$ with $x_{i,j} \in M$.

Use $h=2 \,a_2/N$ with $N=100$ and $0\leq i,j \leq N$. Plot the electrostatic
potential.

## Monte-Carlo integration

The following pseudo-random number generator

$$
 r_n = (r_{n-2} - r_{n-3} - c_{n-1})\,\mbox{mod}\, (2^{32} -18)
$$

with

$$
c_n = \left\{  \begin{array}{cc} 0  & \quad \mbox{for} \quad r_{n-2} - r_{n-3} - c_{n-1} > 0 \\
                                 1   &\quad \mbox{otherwise} \end{array} \right.
$$

provides homogeneously distributed numbers

$$
z_n = r_n/4294967296
$$

in the interval [0,1].

Use this generator to implement a Monte-Carlo-Integration of the electrostatic
potentials {eq}`potential` for $y_0=2$, $x_0 \in [-a_2,a_2]$

$$
\Phi(x_0,2) & = &  \frac{\rho}{4\pi\epsilon_0} \int_{-1}^{1}\int_{-1}^{1}\,
                        \frac{1}{\sqrt{(x-x_0)^2+(y-2)^2}}\,dx\,dy  \\
    & \approx & \frac{\rho}{4\pi\epsilon_0} \,\,\frac{4}{N} 
       \sum_{i=1}^N \frac{1}{\sqrt{(\alpha_x^{(i)}-x_0)^2+{(\alpha_y^{(i)}-2)^2}}},
$$

where $(\alpha_x^{(i)},\alpha_y^{(i)}) \in G_1$ denote evenly distributed
random numbers.  Plot the results of the Monte-Carlo-Integration for (i)
$N=10^2$, (ii) $N=10^3$, (iii) $N=10^5$ and compare to the results from the
Gauss-Legendre integration.
