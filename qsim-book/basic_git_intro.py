#!/usr/bin/env python3
# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:hydrogen
#     notebook_metadata_filter: rise
#     text_representation:
#       extension: .py
#       format_name: hydrogen
#       format_version: '1.3'
#       jupytext_version: 1.10.3
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
#   rise:
#     enable_chalkboard: true
#     theme: white
#     transition: none
# ---

# %% [markdown] slideshow={"slide_type": "slide"} tags=[]
# # Basic introduction to git

# %% [markdown] slideshow={"slide_type": "slide"} tags=[]
# ## Why version control?
#
# <div style="text-align: center;">
#  <br>
#  <img style="width:600px; display: block; margin: 0 auto;" src="images/phd101212s.gif" alt="http://phdcomics.com/comics/archive.php?comicid=1531">
#  <a href="http://phdcomics.com/comics/archive.php?comicid=1531">http://phdcomics.com/comics/archive.php?comicid=1531</a>
# </div>

# %% [markdown] slideshow={"slide_type": "slide"} tags=[]
# ## Why version control?
# Report progress:
#
# - `FINAL.doc`
# - `FINAL_rev2.doc`
# - `FINAL_rev.6.COMMENTS.doc`
# - `FINAL_rev.6.COMMENTS.doc`
# - `FINAL_rev.8.comments5.CORRECTIONS.doc`
# - `FINAL_rev.18.comments7.corrections9.MORE.30.doc`
# - `FINAL_rev.22.comments49.corrections.10.#@$%WHYDIDICOMETOGRADSCHOOL???.doc`
#
# %% [markdown] slideshow={"slide_type": "fragment"} tags=[]
# <div style="text-align: center; color:red; font-size: 1.5em;">
#  How to improve this workflow?
# </div>


# %% [markdown] slideshow={"slide_type": "slide"} tags=[]
# ## History of version control
#
# Local version control
#
# <img style="display: block; margin: 0 auto;" src="images/git-scm_local.png" alt="https://git-scm.com/book/en/v2/images/local.png">
# <br>
# <div style="text-align: center;">
#   Example: <a href="https://de.wikipedia.org/wiki/Revision_Control_System">GNU RCS Revision Control System (1982)</a>
# </div>

# %% [markdown] slideshow={"slide_type": "slide"} tags=[]
# ## History of version control
#
# Centralized version control
#
# <img style="display: block; margin: 0 auto;" src="images/git-scm_centralized.png" alt="https://git-scm.com/book/en/v2/images/centralized.png">
# <br>
# <div style="text-align: center;">
#   Example: <a href="https://de.wikipedia.org/wiki/Concurrent_Versions_System">GNU CVS Concurrent Versions System (1990)</a>, <a href="https://en.wikipedia.org/wiki/Apache_Subversion">Apache Subversion (2000)</a>
# </div>

# %% [markdown] slideshow={"slide_type": "slide"} tags=[]
# ## History of version control
#
# Distributed version control
#
# <img style="height:600px; display: block; margin: 0 auto;" src="images/git-scm_distributed.png" alt="https://git-scm.com/book/en/v2/images/distributed.png">
# <br>
# <div style="text-align: center;">
#   Example: <a href="https://de.wikipedia.org/wiki/BitKeeper">BitKeeper (2000)</a>, <a href="https://de.wikipedia.org/wiki/Mercurial">Mercurial (2005)</a>, <a href="https://de.wikipedia.org/wiki/Git">Git (2005)</a>
# </div>


# %% [markdown] slideshow={"slide_type": "slide"} tags=[]
# ## Why is distributed version control useful?
#
# Combines advantages of *local* and *centralised* version control:
# - Everyone has local history, enabling offline work when no network is available
# - File revisions can be *pushed* to remote servers for sharing
#
# %% [markdown] slideshow={"slide_type": "fragment"} tags=[]
# Difference to centralized version control:
#  - Multiple remote servers are possible
#  - Peer to peer network possible, no centralized server needed

# %% [markdown] slideshow={"slide_type": "slide"} tags=[]
# ## Git repository
# - is the database and contains the history of the project (stores all commits)
# - is located in the . git/ folder inside a project
# - contains other meta-data, logs, and hooks

# %% [markdown] slideshow={"slide_type": "slide"} tags=[]
# ## Basic git usage
#  <div>
#  <br>
#  <img style="width:1200px; display: block; margin: 0 auto;" src="images/basic-usage.svg" alt="basic-usage.svg">
#  </div>

# %% [markdown] slideshow={"slide_type": "slide"} tags=[]
# ## Basic git commands
#  - Create new repository
# ```shell
# git init
# ```
# %% [markdown] slideshow={"slide_type": "fragment"} tags=[]
#  - Add files to staging area
# ```shell
# git add new_file.txt
# ```
# %% [markdown] slideshow={"slide_type": "fragment"} tags=[]
#  - Commit files to repository
# ```shell
# git commit -m "Commit message"
# ```

# %% [markdown] slideshow={"slide_type": "slide"} tags=[]
# ## Basic git commands - inspecting the repository
# %% [markdown] slideshow={"slide_type": "fragment"} tags=[]
#  - Get status of repository
# ```shell
# git status
# ```
# %% [markdown] slideshow={"slide_type": "fragment"} tags=[]
#  - Show history of repository
# ```shell
# git log
# git log --graph --pretty
# ```
# %% [markdown] slideshow={"slide_type": "fragment"} tags=[]
#  - List files at current revision
# ```shell
# git ls-files
# ```

# %% [markdown] slideshow={"slide_type": "slide"} tags=[]
# ## Git in action
#  - Command line examples
#  - <a href="http://onlywei.github.io/explain-git-with-d3/#zen">Visualizing Git Concepts with D3</a>

# %% [markdown] slideshow={"slide_type": "slide"} tags=[]
# ## Git repository stores a directed acyclic graph (DAG)
#
# Examples of a DAG: linear history
#  <div>
#  <br>
#  <img style="display: block; margin: 0 auto;" src="images/git-dag-linear.png" alt="git-dag-linear.png">
#  </div>

# %% [markdown] slideshow={"slide_type": "slide"} tags=[]
# ## Git repository stores a directed acyclic graph (DAG)
#
# Examples of a DAG: branches
#  <div>
#  <br>
#  <img style="display: block; margin: 0 auto;" src="images/git-dag-branches.png" alt="git-dag-branches.png">
#  </div>

# %% [markdown] slideshow={"slide_type": "slide"} tags=[]
# ## Git repository stores a directed acyclic graph (DAG)
#
# Examples of a DAG: branches and branch name
#  <div>
#  <br>
#  <img style="display: block; margin: 0 auto;" src="images/git-dag-branches+branchlabel.png" alt="git-dag-branches+branchlabel.png">
#  </div>

# %% [markdown] slideshow={"slide_type": "slide"} tags=[]
# ## Git repository stores a directed acyclic graph (DAG)
#
# Examples of a DAG: branches and multiple branch names
#  <div>
#  <br>
#  <img style="display: block; margin: 0 auto;" src="images/git-dag-branches+branchlabel2.png" alt="git-dag-branches+branchlabel2.png">
#  </div>

# %% [markdown] slideshow={"slide_type": "slide"} tags=[]
# ## Git repository stores a directed acyclic graph (DAG)
#
# Examples of a DAG: branches, branch names, and HEAD (head of current branch)
#  <div>
#  <br>
#  <img style="display: block; margin: 0 auto;" src="images/git-dag-branches+branchlabel2+head.png" alt="git-dag-branches+branchlabel2+head.png">
#  </div>

# %% [markdown] slideshow={"slide_type": "slide"} tags=[]
# ## Git repository stores a directed acyclic graph (DAG)
#
# Examples of a DAG: tags
#  <div>
#  <br>
#  <img style="display: block; margin: 0 auto;" src="images/git-dag-branches+branchlabel2+head+tag.png" alt="git-dag-branches+branchlabel2+head+tag.png">
#  </div>

# %% [markdown] slideshow={"slide_type": "slide"} tags=[]
# ## Git repository stores a directed acyclic graph (DAG)
#
# Examples of a DAG: merge of branches
#  <div>
#  <br>
#  <img style="display: block; margin: 0 auto;" src="images/git-dag-branches+branchlabel2+head+tag+merge.png" alt="git-dag-branches+branchlabel2+head+tag+merge.png">
#  </div>

# %% [markdown] slideshow={"slide_type": "slide"} tags=[]
# ## Summary: Basic git usage
#  <div>
#  <br>
#  <img style="width:1200px; display: block; margin: 0 auto;" src="images/conventions.svg" alt="conventions.svg">
#  </div>

# %% [markdown] slideshow={"slide_type": "slide"} tags=[]
# ## Visualizing the git DAG
# - git log --graph --pretty
# - tig
# - git cola

# %% [markdown] slideshow={"slide_type": "slide"} tags=[]
# ## Showing differences of revisions
#  <div>
#  <br>
#  <img style="width:1200px; display: block; margin: 0 auto;" src="images/diff.svg" alt="diff.svg">
#  </div>

# %% [markdown] slideshow={"slide_type": "slide"} tags=[]
# ## Showing differences of revisions
# - Show differences between specific git commits and branches
# ```shell
# git diff 1c545e0..7399633
# git diff 1c545e0..HEAD
# git diff HEAD..1c545e0
# git diff HEAD..feature_branch_name
# ```
# - Show differences between working directory and staging area
# ```shell
# git diff
# ```
# - Show differences between staging area and current branch (HEAD)
# ```shell
# git diff --cached
# ```


# %% [markdown] slideshow={"slide_type": "slide"} tags=[]
# ## Basic git commands - handling branches
#  - Creating a branch
# ```shell
# git branch new-branch
# ```
# %% [markdown] slideshow={"slide_type": "fragment"} tags=[]
#  - Listing branches
# ```shell
# git branch -av
# ```
# %% [markdown] slideshow={"slide_type": "fragment"} tags=[]
#  - Switching to a branch or revision
# ```shell
# git checkout new-branch
# ```
# %% [markdown] slideshow={"slide_type": "fragment"} tags=[]
#  - Creating branch and switching right away to this branch
# ```shell
# git checkout -b new-branch
# ```

# %% [markdown] slideshow={"slide_type": "slide"} tags=[]
# ## Basic git commands - handling tags
#
# %% [markdown] slideshow={"slide_type": "fragment"} tags=[]
#  - Certain git commits might have a special relevance
#    - release version of a code
#    - version sent to collaborators, peer review, etc.
# %% [markdown] slideshow={"slide_type": "fragment"} tags=[]
#  - Creating a tag
# ```shell
# git tag v_1.0 -m 'v_1.0'
# ```
#  - Listing tags
# ```shell
# git tag -l
# ```
# %% [markdown] slideshow={"slide_type": "fragment"} tags=[]
#  - Checkout of tagged revision
# ```shell
# git checkout v_1.0
# ```

# %% [markdown] slideshow={"slide_type": "slide"} tags=[]
# ## Merging branches
# - Two different branches can be combined with a merge. The
#  current position always has to be on the receiving side for this.
# This merges the feature branch into the master branch.
# ```shell
#  git checkout master
#  git merge feature_branch
# ```
# To achieve
# the opposite and to merge the master branch into the feature_branch
# we first have to switch to the feature branch and then merge
# ```shell
#  git checkout feature_branch
#  git merge master
# ```
# - Such a merge creates a merge commit which has two parents

# %% [markdown] slideshow={"slide_type": "slide"} tags=[]
# ## Git merges: resolving conflicts
# - Conflict resolution is concerned with selecting the correct change
# - Create version of text/code that is correct
# - Correct can be remote changes, local changes, or a mix of both
# - Remove conflict markers
# - Add and commit the affected files

# %% [markdown] slideshow={"slide_type": "slide"} tags=[]
# ## Git merges: Example for handling conflicts
# - Conflicts appear when local changes are different from remote changes
# - Example of conflict
# ```shell
#  <<<<<<< HEAD
#  Hi World
#  =======
#  Hello World
#  >>>>>>> e4d7749dcfe0c5c9694afcf20f1dce17d025d6fe
#  ```
# - To resolve the conflict, remove the conflict markers, adapt
#   the text or code accordingly, and then
# ```shell
#  > git add affected_files
#  > git commit
# ```
# - Detect forgotten conflict markers
# ```shell
#  > git diff --check
# ```

# %% [markdown] slideshow={"slide_type": "slide"} tags=[]
# ## Git rebase
# - Rebasing is the process of moving or combining a sequence of commits to a new base commit.
#  <div>
#  <br>
#  <img style="width:1200px; display: block; margin: 0 auto;" src="images/rebase.svg" alt="rebase.svg">
#  </div>

# %% [markdown] slideshow={"slide_type": "slide"} tags=[]
# ## Interactive rebase
# - Create repository with some commits
#
# ```shell
# git init rebase_example
# cd rebase_example
# for x in $(seq 1 5); do
#   echo $x > $x.txt;
#   git add $x.txt;
#   git commit $x.txt -m "commit #$x";
#  done
# ```
#
# ```shell
# git log
# * 2422c2d <Heiko Appel> (2021-05-10 21:50:17 +0200)  (HEAD -> refs/heads/master)commit #5
# * c593588 <Heiko Appel> (2021-05-10 21:50:17 +0200) commit #4
# * b1a172e <Heiko Appel> (2021-05-10 21:50:17 +0200) commit #3
# * 426729a <Heiko Appel> (2021-05-10 21:50:17 +0200) commit #2
# * 8ed44c4 <Heiko Appel> (2021-05-10 21:50:17 +0200) commit #1
# ```


# %% [markdown] slideshow={"slide_type": "slide"} tags=[]
# ## Interactive rebase
#
# <pre>
# pick 426729a commit #2
# pick b1a172e commit #3
# pick 36006b2 commit #4
# pick 5b94837 commit #5
#
# # Rebase 8ed44c4..36006b2 onto 8ed44c4 (4 commands)
# #
# # Commands:
# # p, pick <commit> = use commit
# # r, reword <commit> = use commit, but edit the commit message
# # e, edit <commit> = use commit, but stop for amending
# # s, squash <commit> = use commit, but meld into previous commit
# # f, fixup <commit> = like "squash", but discard this commit's log message
# # x, exec <command> = run command (the rest of the line) using shell
# # b, break = stop here (continue rebase later with 'git rebase --continue')
# # d, drop <commit> = remove commit
# # l, label <label> = label current HEAD with a name
# # t, reset <label> = reset HEAD to a label
# # m, merge [-C <commit> | -c <commit>] <label> [# <oneline>]
# </pre>

# %% [markdown] slideshow={"slide_type": "slide"} tags=[]
# ## Working with remote repositories - part I
# - Creating a bare repository
# ```shell
# git init --bare test.git
# # chmod -R g=u test.git
# ```
# - Cloning remote repository
# ```shell
# git clone path/to/test.git
# git clone git@gitlab.com:heiko.appel/qsim.git
# ```
# - Show configuration of remote repository
# ```shell
# git remote -v
# git remote show origin
# ```
# - git clone creates automatically creates a remote connection called
#    *origin* pointing back to the cloned repository

# %% [markdown] slideshow={"slide_type": "slide"} tags=[]
# ## Working with remote repositories - part II
# - Pushing local changes to remote repository
# ```shell
# git push
# ```
# - Getting changes from remote repository
# ```shell
# git pull
# git pull --rebase
# ```
# - Show branches
# ```shell
# git branch -av
# ```

# %% [markdown] slideshow={"slide_type": "slide"} tags=[]
# ## Revisiting git pull
# - Only retrieving objects from remote git repository
# ```shell
# git fetch
# ```
# - Merging remote branch
# ```shell
# git merge remotes/origin/master
# ```
# - git pull combines the above two steps in a single command

# %% [markdown] slideshow={"slide_type": "slide"} tags=[]
# ## Git actions: overview
#
# <img style="display: block; margin: 0 auto;" src="images/workflow.png" alt="https://www.reddit.com/r/git/comments/99ul9f/git_workflow_diagram_showcasing_the_role_of/">

# %% [markdown] slideshow={"slide_type": "slide"} tags=[]
# ## Handling remotes
# - Add and fetch new remote
# ```shell
# git remote add octopus git@gitlab.com:octopus-code/octopus.git
# git fetch octopus
# ```
# - Configure remote tracking branch
# ```shell
# git branch --set-upstream-to=origin/master master
# git branch --track remotes/octopus/master
# ```
# - Pull (=Fetch+Merge) unrelated histories
# ```shell
#  git pull --allow-unrelated-histories
# ```

# %% [markdown] slideshow={"slide_type": "slide"} tags=[]
# ## Git cherry-pick
# - sometimes it is desirable to select only a few changes from a remote branch
# - git cherry-pick allows to apply individual or ranges of commits to the current branch
# ```shell
#  git cherry-pick 5f1216a
# ```
# - If a file exists only remotely, git complains
# ```shell
#  deleted by us:   README.md
# ```
# - This file has then to be added and the cherry pick can continue
# ```shell
#  git add README.md
#  git cherry-pick --continue
# ```

# %% [markdown] slideshow={"slide_type": "slide"} tags=[]
# ## Building blocks of a git commit
#
# File tree to be stored in git database
# ```shell
# > tree
# .
# ├── AUTHORS.rst
# ├── README.rst
# ├── setup.cfg
# ├── setup.py
# ├── src
# │   └── qsim
# │       ├── __init__.py
# │       └── skeleton.py
# └── tests
#     ├── conftest.py
#     └── test_skeleton.py
# ```
#
# Meta-data connected with file tree
# ```shell
# commit 441c823 (HEAD -> refs/heads/master)
# Author: Heiko Appel &lt;heiko.appel@mpsd.mpg.de&gt;
# Date:   Sat May 8 21:25:10 2021 +0200
#
#    Initial commit
# ```

# %% [markdown] slideshow={"slide_type": "slide"} tags=[]
# ## Inspecting commits, trees, and blobs
# ```shell
# git cat-file -p 441c823
# ```
#
# ## Where do git hashes come from?
# ```shell
# (printf "commit %s\0" $(git cat-file commit HEAD | wc -c); git cat-file commit HEAD) | sha1sum
# ```

# %% [markdown] slideshow={"slide_type": "slide"} tags=[]
# ## Creating Patch files
# - git allows to create text-based patch files from commits
# ```shell
# git format-patch -4 HEAD
# ```
# - The generated patch files can then be
#    - sent to collaborators
#    - copied to some other repository
# - and then added to the other repository via
# ```shell
# git am 0001-Feature-Hotfix.patch
# ```

# %% [markdown] slideshow={"slide_type": "slide"} tags=[]
# ## Various other useful git commands
#  - Find out who made changes in a file
# ```shell
# git blame README.md
# git blame -L 10,20 README.md
# git blame -L 10,+10 README.md
# ```
#  - Print statistics of commits
# ```shell
# git shortlog -sn
# ```
#  - Searching for faulty commits with git bisect
# ```shell
# git bisect start
# git bisect bad
# git bisect good some_tag
# ...
# git bisect bad
# ```

# %% [markdown] slideshow={"slide_type": "slide"} tags=[]
# ## Credits
#  - Some images taken from
#    - <a href="https://git-scm.com/book/de/v2">https://git-scm.com/book/de/v2</a>
#    - <a href="https://www.atlassian.com/git/tutorials">https://www.atlassian.com/git/tutorials</a>
#    - <a href="https://marklodato.github.io/visual-git-guide/index-en.html">https://marklodato.github.io/visual-git-guide/index-en.html</a>
#    - <a href="https://www.reddit.com/r/git/comments/99ul9f/git_workflow_diagram_showcasing_the_role_of">https://www.reddit.com/r/git/comments/99ul9f/git_workflow_diagram_showcasing_the_role_of/</a>
