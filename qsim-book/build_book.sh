#!/bin/bash

toplevel=$(git rev-parse --show-toplevel)

pushd $toplevel
  pushd qsim-book
    for pysrc in $(ls *.py solutions/*.py); do
      jupytext --sync $pysrc
    done
    # cleanup old build artifacts
    rm -rf _build ../public
    cp -a ../README.md intro.md
    cp -a ../INSTALL.md qsim_installation.md
  popd
 # build the jupyter book
 jupyter-book build qsim-book
 # move the build artifacts to the public folder
 rm -rf public
 mv qsim-book/_build/html public
 rsync -av qsim-book/images/ public/images
 rsync -av qsim-book/assignments/*.dat public/assignments
 chmod -R ugo+rX public
popd
