# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:hydrogen
#     text_representation:
#       extension: .py
#       format_name: hydrogen
#       format_version: '1.3'
#       jupytext_version: 1.10.3
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# %% [markdown]
# # Solution of the quadratic equation
#
# We are interested in finding the solution of a quadratic equation, which reads as
#
# $$ ax^2 + bx+c =0\,, $$
#
# where numbers $a$, $b$, and $c$ are the coefficients of the equation.
#
# ## Analytical solutions
# The solution of the quadratic equation are usually obtained using the process of completing the square, that makes use of the identity
#
# $$x^2+2hx+h^2 = (x+h)^2\,.$$
#
# From this, we directly get the solution of the quadratic equation
#
# $$ x = \frac{-b \pm \sqrt{b^2-4ac}}{2a}\,.$$
#
# Another possible form of the solutions are given by the so-called Vieta's formulas
#
# $$ x = \frac{2c}{-b \pm \sqrt{b^2-4ac}}\,.$$
#
# ## Numerical solutions
# Let us now look at the specific case of
# %% [markdown]
#

# %%
import matplotlib.pyplot as plt
import numpy as np

a = 1e-20
b = 1
c = 1


# %% [markdown]
# From these parameters, it is clear that one solution is close $x_1\approx-1$.
# The second solution is close to $x_2\approx-10^{20}$.
# We can check this graphically:
# %% [markdown]
#

# %%
def f(x):
    return a * x ** 2 + b * x + c


# %%
x_values = np.arange(-1.001, -0.999, 1e-6)
f_values = list(map(f, x_values))
y0 = np.zeros(2000)
plt.plot(x_values, f_values, label="f(x)=ax^2+bx+c")
plt.plot(x_values, y0, "--k", label="y=0")
plt.title("Graphical solution $x_1$ of the quadratic equation")
plt.xlim([-1.001, -0.999])
plt.xticks([-1.001, -1, -0.999])
plt.legend()
plt.show()

x_values = np.arange(-1.5e20, -0.5e20, 1e18)
f_values = list(map(f, x_values))
y0 = np.zeros(100)
plt.plot(x_values, f_values, label="f(x)=ax^2+bx+c")
plt.plot(x_values, y0, "--k", label="y=0")
plt.title("Graphical solution $x_2$ of the quadratic equation")
plt.xlim([-1.5e20, -0.5e20])
plt.xticks([-1.5e20, -1e20, -0.5e20])
plt.legend()
plt.show()

# %% [markdown]
# Let us now see what the four exact analytical formula give.
# First we use the standard formula
# %% [markdown]
#


# %%
def standard_quadratic_solutions():
    discr = np.sqrt(b ** 2 - 4 * a * c)
    return [(-b + discr) / (2 * a), (-b - discr) / (2 * a)]


# %%
standard_quadratic_solutions()

# %% [markdown]
# We are getting only of the solution correct.
#
# Let us now look at the result from the Vitea's formula:
# %% [markdown]
#


# %%
def vitea_quadratic_solutions():
    discr = np.sqrt(b ** 2 - 4 * a * c)
    return [2 * c / (-b + discr), 2 * c / (-b - discr)]


# %%
vitea_quadratic_solutions()

# %% [markdown]
# Clearly, we are getting different results. In fact we are finding the second solution that we want, $x_1=-1$, and an incorrect result.
#
# This example illustrates how exact analytical results can produce completely wrong results, due to finite precision and rounding errors.
#
# ## Stable numerical solutions
#
# We now analyze why we are getting these results. In the case of the standard formula, we are computing
#
# $$ -1 + \sqrt{1-4\times10^{-20}} \approx -1 + \sqrt{1} = -1 + 1 = 0 \,.$$
#
# This is why we are getting a wrong solution. The same problem occur with one of the Vitea's formula, leading to the division by zero.
#
# The correct way to compute the solution is in fact to compute the quantity
#
# $$ q = -\frac{1}{2}\Big[b + \mathrm{sgn}(b)\sqrt{b^2-4ab} \Big]\,,$$
#
# and to use one solution from the standard formula, and one from the Vitea's formula
#
# $$x_1 = \frac{q}{a} \qquad \mathrm{and} \qquad x_2 = \frac{c}{q} $$
#
# Doing so, we are never substracting to $b$ a very nearly equal quantity (the discriminant) and we now get precise solutions.
#
# %% [markdown]
#


# %%
def stable_quadratic_solutions():
    q = -0.5 * (b + np.sign(b) * np.sqrt(b ** 2 - 4 * a * c))
    return [q / a, c / q]


# %%
stable_quadratic_solutions()

# %% [markdown]
# Finally, we note that none of these results are formally correct. Indeed, $x=-1$ cannot be one of the solution of the equation, as this would correspond to the case $a=0$.
#
# These results are however numerically correct, in the sense that they are the closest possible number to the exact result that can be represented on the precision of the computer.
#
# %% [markdown]
#
