"""
Exercice 02
(c) 2004 Heiko Appel
(c) 2021 Nicolas Tancogne-Dejean
(c) 2021 Martin Lueders
"""
import math

# 2.1

def f1(x):
    return 1/(1+2*x) - (1-x)/(1+x)

def f2(x):
    return (2*x**2)/(2*x**2+3*x+1)


def g1(x):
    return (1-math.cos(x))/x

def g2(x):
    return (2*math.sin(x/2)**2)/x



# 2.2 Numerical derivatives

# Part (i): f(x) = x^3, f'(x) = 3x^2


def f_i(x):
    return x * x * x


def fprime_i(x):
    return 3 * x * x


# Part (ii): f(x) = exp(x), f'(x) = exp(x)


def f_ii(x):
    return math.exp(x)


def fprime_ii(x):
    return math.exp(x)


# Difference quotient for part (a), respectively for the functions (i) and (ii)

def diffquot_f(func, x, h):
    """
    Return the forward difference quotient for function 'func' at x with small parameter h
    """
    return (func(x+h) - func(x))/h

def diffquot_c(func, x, h):
    """
    Return the forward difference quotient for function 'func' at x with small parameter h
    """
    return (func(x+h) - func(x-h))/(2*h)

