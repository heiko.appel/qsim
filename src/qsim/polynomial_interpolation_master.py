"""
Exercice 03
(c) 2004, 2021 Heiko Appel
(c) 2021 Nicolas Tancogne-Dejean
(c) 2021 Martin Lueders
"""
import math

import numpy as np

# function to be interpolated

def f(x):
    return 5.0 / (1.0 + 25 * x * x)


def create_interval_i(n):
    """
    Generate a equally spaced mesh in with n+1 points in the interval [-1:1].

    Input:
        n: number of grid points.

    Returns:
        x: array of length n+1 mesh points
    """

    x = np.zeros(n+1)

    # Define interval boundaries
    a = -1
    b = 1

    # step width for nodes of form (i)
    h = (b - a) / n

    for i in range(0, n + 1):
        x[i] = a + i * h  # nodes of form (i)

    return x

def create_interval_ii(n):
    """
    Generate a mesh in with n+1 points in the interval [-1:1] according to method (ii).

    Input:
        n: number of grid points.

    Returns:
        x: array of length n+1 mesh points
    """

    x = np.zeros(n+1)

    for i in range(0, n + 1):
        x[i] = math.cos((2 * i + 1) * np.pi / ((n + 1) * 2)) # nodes of form (ii)

    return x


def interpolate(func, nodes):
    """
    Generate the coefficients for the Newtonian interpolating polynomial for the function 'func' using the given nodes.

    Input:
        func: function to be interpolated
        nodes: grid of nodal points

    Returns:
        array of coefficients
    """

    n = len(nodes)
    coeff = np.zeros(n)
    d = np.zeros(n)

    # calculation of the coefficients for the Newton form
    for i in range(0, n):
        d[i] = func(nodes[i])
        for j in range(i - 1, -1, -1):
            d[j] = (d[j + 1] - d[j]) / (nodes[i] - nodes[j])
        coeff[i] = d[0]

    return coeff


def horner(nodes, coeffs, x):
    """
    Horner scheme for evaluating the polynomial

    Input:
        nodes:  array with nodal points
        coeffs: coefficients of the polynomial
        x:      argument at which to evaluate the polynomial

    Returns
        value of the polynomial
    """

    assert len(nodes) == len(coeffs)

    n = len(nodes)
    p = coeffs[n-1]
    for j in range(n - 2, -1, -1):
        p = p * (x - nodes[j]) + coeffs[j]

    return p
