"""
Utilities
(c) 2021 Martin Lueders
"""

from IPython.display import display, Markdown, Latex
import inspect

def display_source(func):

    display(Markdown("```python\n"+inspect.getsource(func)+"```"))