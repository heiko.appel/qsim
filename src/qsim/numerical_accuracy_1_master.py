"""
Exercice 01
(c) 2004 Heiko Appel
(c) 2021 Nicolas Tancogne-Dejean
(c) 2021 Martin Lueders
"""

import math
import numpy as np

def arctan_approx_series_simple(x, precision, verbose=False, n1=100000000):
    """
    Approximate arctan(x) to given precision.

    Input: 
        x:         argument at which arctan() shall be evaluated
        precision: desired precision

    Returns: tuple of three values:
        (approximation of arctan(x) to given precision, relative error, number of terms)

    Evaluation by direct summation of the series
    S_n(x) =  \sum_{\nu=0}^{n-1} (-1)^{\nu} \frac{x^{2\nu+1}}{2\nu+1} 
    """

    # initial values

    def term(x, n):
        return (-1)**n * x**(2*n+1) / (2*n + 1)

    arctan_x = x
    n= 1
    exact = np.arctan(x)

    # initial relative error (Starting values)
    if x==0:
        delta = 0
    else:
        delta = abs((arctan_x - exact) / exact)

    if verbose:
        print("Series expansion")
        print("Terms      Approximation for arctan(1) relative error")
        print("{:d} \t  {:>15f}\t  {:>17.10f}".format(n, arctan_x, delta))

    while delta > precision:  # Continue the loop while relative error > 10^{-6}
        arctan_x += term(x, n)  # sum the terms
        n += 1
        delta = abs((arctan_x - exact) / exact)  # calculate error

        if verbose:
            if n < 10 or n > n1:  # print some of the steps
                print("{:d} \t  {:>15f}\t  {:>17.10f}".format(n, arctan_x, delta))
            if n == 10 and n1>10:
                print("\n   ... \t   ... \t\t\t    ...\n")

    return (arctan_x, delta, n)



def arctan_approx_series(x, precision, verbose=False, n1=100000000):
    """
    Approximate arctan(x) to given precision.

    Input: 
        x:         argument at which arctan() shall be evaluated
        precision: desired precision

    Returns: tuple of three values:
        (approximation of arctan(x) to given precision, relative error, number of terms)

    Evaluation by optimized summation of the series
    S_n(x) =  \sum_{\nu=0}^{n-1} (-1)^{\nu} \frac{x^{2\nu+1}}{2\nu+1} 
    """

    # initial values

    power_x = x  # Arguments for which the series is to be calculated
    sign = 1  # Variable for the alternating sign
    loop = 1  # Counter for the loop iterations
    denominator = 1  # Denominator of the series terms
    arctan_x = x

    # initial relative error (Starting values)
    exact = np.arctan(x)

    if x==0:
        delta = 0
    else:
        delta = abs((arctan_x - exact) / exact)

    if verbose:
        print("Series expansion")
        print("Terms      Approximation for arctan(1) relative error")
        print("{:d} \t  {:>15f}\t  {:>17.10f}".format(loop, arctan_x, delta))

    while delta > precision:  # Continue the loop while relative error > 10^{-6}
        loop += 1  # count the terms
        sign *= -1  # update alternating sign
        denominator += 2  # increment denominator in steps of two
        power_x *= x * x  # Increase the power of the numerator by two
        arctan_x += sign * power_x / denominator  # sum the terms
        delta = abs((arctan_x - exact) / exact)  # calculate error

        if verbose:
            if loop < 10 or loop > n1:  # print some of the steps
                print("{:d} \t  {:>15f}\t  {:>17.10f}".format(loop, arctan_x, delta))
            if loop == 10 and n1 > 10:
                print("\n   ... \t   ... \t\t\t    ...\n")

    return (arctan_x, delta, loop)


def arctan_approx_means(x, precision, verbose=False, n1=100000000):
    """
    Approximate arctan(x) to given precision.

    Input: 
        x:         argument at which arctan() shall be evaluated
        precision: desired precision

    Returns: tuple of three values:
        (approximation of arctan(x) to given precision, relative error, number of terms)

    Evaluation by arithmetic-geometric means:
    """

    # initial values

    loop = 1  # Counter for the loop iterations
    arctan_x = x
    an = 1 / math.sqrt(1 + x * x)  # Initialise the iteration
    bn = 1

    exact = np.arctan(x)
    # initial relative error (Starting values)
    delta = abs((arctan_x - exact) / exact)

    if verbose:
        print("Series expansion")
        print("Terms      Approximation for arctan(1) relative error")
        print("{:d} \t  {:>15f}\t  {:>17.10f}".format(loop, arctan_x, delta))

#    while delta > precision:  # Continue the loop while relative error > 10^{-6}
    while abs(an - bn) > precision:
        loop += 1  # count the terms
        an = 0.5 * (an + bn)  # Iteration
        bn = math.sqrt(an * bn) # note that here an already has been updated
        arctan_x = x / (math.sqrt(1 + x * x) * an)
        delta = abs((arctan_x - exact) / exact)

        if verbose:
            if loop < 10 or loop > n1:  # print some of the steps
                print("{:d} \t  {:>15f}\t  {:>17.10f}".format(loop, arctan_x, delta))
            if loop == 10 and n1 > 10:
                print("\n   ... \t   ... \t\t\t    ...\n")

    return (arctan_x, delta, loop)


def eps(alpha):
    """
    Calculates the machine precision

    Input:
        alpha: starting value

    Returns:
        (machine precision, number of divisions)
    """

    eps = 1  # eps=1; Step 1 of the Pseudo Code
    i = 1
    end = 1
    while end:
        eps /= 2  # Divide eps by two; Step 2 of the Pseudo Code
        i += 1
        # Count how often we divide ...

        if (alpha + eps) > alpha:
            end = 1
        else:
            end = 0

    eps *= 2
    # Double eps (Step 3)
 
    return (eps, i)


